# Contributing to Any Shop Co-op Infrastructure

We're thrilled that you're interested in contributing to the Any Shop Co-op Infrastructure! This document provides guidelines for contributions to help ensure a smooth collaboration process for everyone involved.

## Getting Started

Before you begin contributing, please ensure you have the following:

- A [GitLab account](https://gitlab.com/)
- Familiarity with [Nix and NixOS](https://nixos.org/learn.html)
- Basic understanding of Git workflows

## How to Contribute

### Reporting Issues

If you encounter any bugs, inconsistencies, or missing features in the infrastructure setup, please file an issue using the following steps:

1. Check the [Issue Tracker](https://gitlab.com/any-shop-co-op/infrastructure/-/issues) to see if the problem has already been reported.
2. If the issue is new, click the "New Issue" button and provide a clear description of the problem, including relevant details such as:
   - The environment in which you encountered the issue (e.g., NixOS version, specific configurations)
   - Steps to reproduce the issue
   - Expected behavior versus actual behavior
   - Any relevant error messages or logs

### Submitting Changes

To contribute code or documentation changes, please follow these steps:

1. Fork the repository on GitLab.
2. Clone your fork to your local machine.
3. Create a new branch for your changes: `git checkout -b my-feature-branch`
4. Make your changes, commit them, and push the branch to your fork: `git push -u origin my-feature-branch`
5. Visit the repository page on GitLab and click "Merge Request" to submit your changes for review.
6. Provide a clear description of your changes and any additional context that may help the reviewers.

### Code and Commit Guidelines

- Ensure your code adheres to the project's style and standards.
- Write clear, concise commit messages, following best practices such as:
  - Using the imperative mood in the subject line
  - Including a meaningful message body if necessary to explain what and why, not just how

## Review Process

After you submit a merge request, the project maintainers will review your changes. They may provide feedback or request additional modifications before merging your contribution. Please be patient and responsive to any questions or suggestions during the review process.

## Community Guidelines

We are committed to fostering a welcoming and respectful community. As a contributor, you are expected to:

- Be respectful of others, their contributions, and their opinions.
- Follow the project's code of conduct.
- Collaborate openly and constructively.

Thank you for contributing to the Any Shop Co-op Infrastructure! Your efforts help build a stronger, more collaborative platform for everyone.